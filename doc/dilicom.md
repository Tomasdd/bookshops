### Dilicom connector

In addition to basic fields, it also returns:

- price excl. VAT (prix HT)
- VAT (TVA1)
- distributor GLN
- theme
- collection
- thickness, height, width, weight
- présentation éditeur (relié, broché, poche, disque vinyle etc)
- présentation magasin (posé en linéaire)