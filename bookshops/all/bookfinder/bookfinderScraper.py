#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
import requests
from bs4 import BeautifulSoup
from sigtools.modifiers import annotate
from sigtools.modifiers import kwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import isbn_cleanup
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)

"""
Nice results, except: we can't find the books' ISBN in a regular
search. We need a post search, with which we get more data: publication date, format, summary…
"""


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Internal datasource name.
        self.SOURCE_NAME = "bookfinder"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.bookfinder.com"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.bookfinder.com/search/?keywords="
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = "https://www.bookfinder.com/isbn/"

        #: Optional suffix to the search url
        #: Here: don't show e-books.
        #: To apply only for a words search (not ISBN search).
        # self.URL_END = ""
        self.URL_END = "/?st=sr&ac=qr&mode=basic&lang=en&destination=us&currency=USD&binding=*&publisher=&min_year=&max_year=&minprice=&maxprice=&classic=off"
        self.TYPE_BOOK = "book"

        #: advanced url (search for isbns)
        #: Here: the normal search works for ISBNs, we are redirected to the product page.
        self.SOURCE_URL_ADVANCED_SEARCH = ""

        #: Unused:
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 28

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []

        # ISBN search: same results page.
        # Words search:
        try:
            plist = self.soup.find_all(class_='bf-content-header')
            # ebooks are excluded with the search URL.
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []
            return plist

        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        # return self.NBR_RESULTS
        return None

    @catch_errors
    def _details_url(self, product):
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn
        details_url = product.find(class_='bf-content-header-book-title').find('a').attrs.get('href')
        return self.SOURCE_URL_BASE + details_url

    @catch_errors
    def _title(self, product):
        title = product.find(class_='bf-content-header-book-title').text
        return title

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        # Expecting a list, for multiple authors.
        authors = product.find(class_='bf-content-header-book-author').find_all('a')
        authors = [it.text.strip() for it in authors]
        return authors

    @catch_errors
    def _img(self, product):
        img = product.find(class_='bf-content-header-img').attrs.get('src')
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        # present in ISBN search.
        # XXX:
        return []

    def _price(self, product):
        ""
        if not self.isbn:
            return None

        prices_url = "https://www.bookfinder.com/isbnresults/{}/?ac=qr&binding=*&currency=EUR&destination=us&keywords={}&lang=en&max_year=&maxprice=&min_year=&minprice=&mode=basic&publisher=&st=sr".format(self.isbn, self.isbn)
        try:
            # For an ISBN search.
            # We need a second network request.
            req = requests.get(
                prices_url,
                headers=self.HEADERS,
                timeout=self.TIMEOUT,
            )

            log.info("launching second network request to get price data...")
            soup = BeautifulSoup(req.content, "lxml")
            cols = soup.find_all(class_='bf-search-result-col-actions')
            #.find(class_='clickout-logger').text
            price = None
            for col in cols:
                clickout = col.find(class_='clickout-logger')
                if clickout and clickout.text:
                    price = clickout.text
                    break

            if price:
                price = priceFromText(price)
                price = priceStr2Float(price)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn

        # WARN! No ISBN in keyword search results !
        # It's just a list of links.
        return None

        res = product.find("a")
        isbn = res.attrs.get('href').strip()
        isbn = isbn.split('/')[-1]
        isbn = isbn.split('.html')[0]
        if not is_isbn(isbn):
            logging.warning('Italian Hoepli scraper: our ISBN is not an ISBN: {}'.format(isbn))
            return ""
        return isbn

    @catch_errors
    def _description(self, product):
        pass

    @catch_errors
    def _date_publication(self, product):
        """
        Return: a string (not a date object).
        """
        # XXX: doable.
        return

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        # missing
        return

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        pass

    def search(self, *args, **kwargs):
        """
        Searches books. Returns a list of books.

        This scraper works with an ISBN search.

        From keywords: todo.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()

        if product_list:
            simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)

        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            #: language
            b.lang = "Português"

            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        return bk_list, stacktraces


def postSearch(details_url, isbn=None, card={}):
    """
    Get a card (dictionnary) with 'details_url'.

    - details_url: url (str)
    - isbn: isbn (str)
    - card: dict

    Gets additional data:
    - isbn (after a keywords search)
    - publication date
    - etc

    Return a new card (dict) complemented with the new attributes.
    """
    # An ISBN search with the default endpoints redirects to the product page.
    # Looks like we can use the search() method to get a book's details
    # url = details_url or card.get('details_url') or card.get('url')
    if not url:
        log.error("postSearch error: url is False ! ({}).".format(url))
        return None

    # req = requests.get(url)
    # soup = BeautifulSoup(req.text, "lxml")

    return card


@annotate(words=clize.Parameter.REQUIRED)
@kwoargs("postsearch", "nb")
def main(postsearch=False, nb=100, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    # ISBN is missing from a keywords search :/
    if postsearch:
        if nb:
            bklist = bklist[:int(nb)]
        bklist = [
            postSearch(
                it.get('details_url'),
                isbn=it.get('isbn'),
                card=it,
            )
            for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
