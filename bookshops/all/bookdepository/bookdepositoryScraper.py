#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "bookdepository"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.bookdepository.com"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.bookdepository.com/search?search=Find+book&searchTerm=+"  # http://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=simple&LIVREANCIEN=2&MOTS="
        #: advanced url (search for isbns)
        # self.SOURCE_URL_ADVANCED_SEARCH = "http://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=appro&LIVREANCIEN=2&MOTS="
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 30

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        """
        Return: a list of dom/soup elements holding data of a search result.
        """
        if is_isbn(self.query):
            try:
                # If we got redirected to 1 result page:
                plist = self.soup.find(class_='item-wrap')
                if not plist:
                    logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                    return []
                return [plist]
            except Exception as e:
                logging.error("Error while getting product list. Will return []. Error: {}".format(e))
                return []

        # Normal keyword search.
        try:
            plist = self.soup.find_all(class_='book-item')
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []

            return plist
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        pass
        # try:
        #     nbr_resultl_list = self.soup.find_all('div', class_='result_position')
        #     nbr_result = nbr_resultl_list[0].text.strip()
        #     res = nbr_result.split('sur')[-1]
        #     if not res:
        #         print('Error matching nbr_result')
        #     else:
        #         nbr = int(res.strip())
        #         self.NBR_RESULTS = nbr
        #         return nbr
        # except Exception as e:
        #     logging.info("Could not fetch the nb of results: {}".format(e))

    @catch_errors
    def _details_url(self, product):
        if is_isbn(self.query):
            return self.req.url  # we probably got redirected to the book's page.

        details_url = product.find(class_="item-img").a['href']
        details_url = self.SOURCE_URL_SEARCH + details_url
        return details_url

    @catch_errors
    def _title(self, product):
        if is_isbn(self.query):
            title = product.find(class_="item-info").find('h1').text.strip()
            return title

        return product.find(class_="title").text.strip()

    @catch_errors
    def _authors(self, product):
        """
        Return a list of str.
        """
        # Case on book page:
        try:
            if is_isbn(self.query):
                authors = product.find(class_="author-info").find(itemprop="name").text  # XXX: many authors?
                authors = authors.strip()
                # authors = authors.split('\n')
                # TODO: multiple authors
                # authors = [it for it in authors if it != ""]
                # authors = [it.strip().capitalize() for it in authors]
                return [authors]
        except Exception as e:
            logging.warning(e)
            return []

        authors = product.find(class_="author").text.strip()
        return [authors]

    @catch_errors
    def _img(self, product):
        if is_isbn(self.query):
            img = product.find(class_='book-img')['src']
            return img

        img = product.find(class_="item-img").img['data-lazy']
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        if is_isbn(self.query):
            try:
                # Case of book page:
                pub = product.find(itemprop="publisher").text.strip()
                # TODO: multiple publishers ?
                return [pub]
            except Exception as e:
                logging.error("Error scraping the publisher(s): {}".format(e))

        # pas d'info éditeur sur les résultats de recherche par mot-clef !
        return []

    @catch_errors
    def _price(self, product):
        """
        The real price (as float), without discounts.
        """
        if is_isbn(self.query):
            try:
                price = product.find(class_='list-price').text.strip()
                price = priceFromText(price)
                price = priceStr2Float(price)
                return price
            except Exception as e:
                print(('Erreur getting price {}'.format(e)))
                return

        # Sometimes there is no price.
        # price-wrap is always there, price not always.
        # When no price, there "unavailable" in a div.
        # How to say that in Abelujo?
        price = product.find(class_="price")
        if not price:
            return
        # Sometimes there is a reduction, we want the original price.
        original = price.find(class_="rrp")
        if original:
            price = priceFromText(original.text.strip())
            price = priceStr2Float(price)
            return price
        price = priceFromText(price.text.strip())
        price = priceStr2Float(price)
        return price

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        if is_isbn(self.query):
            return self.query

        # L'ISBN est à trouver sur les URLs.
        href = product.find(class_="title").a['href']
        isbn = href.split('?')[0].split('/')[-1]
        if not is_isbn(isbn):
            logging.warning("Could not find ISBN in this link: {}".format(href))
        return isbn

    @catch_errors
    def _description(self, product):
        if is_isbn(self.query):
            res = product.find(itemprop="description").text.strip()
            res = res.replace('show more', '')
            return res

        # Pas de description dans les résultats par mot-clef, normal.
        return

    @catch_errors
    def _details(self, product):
        # looks deprecated.
        pass

    @catch_errors
    def _date_publication(self, product):
        # Case on book page:
        if is_isbn(self.query):
            date_publication = product.find(itemprop="datePublished")
            if date_publication:
                date_publication = date_publication.text.strip()
            return date_publication

        date_publication = product.find(class_="published").text.strip()
        return date_publication

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        # XXX: very fragile. No better structure.
        if is_isbn(self.query):
            availability = product.find(class_="checkout-tools").find_all('p')
            if availability:
                availability = availability[1]
                if availability:
                    availability = availability.text.strip()
                    return availability

        return

    @catch_errors
    def _dimensions(self, product):
        """
        Épaisseur, hauteur, largeur, poids.
        Return: tuple of numbers.
        """
        if not is_isbn(self.query):
            return [None, None, None, None]

        res = product.find(class_="biblio-info").find_all('li')[1]
        label = res.label.text.strip().lower()
        if "dimensions" not in label:
            return
        res = res[1].span.text
        res = [it.strip() for it in res.split('mm')]  # dimensions, poid
        res = res[0]
        res = res.split('x')
        res = [it.strip() for it in res]
        res = [priceFromText(it) for it in res]
        res = [priceStr2Float(it) for it in res]
        largeur, hauteur, epaisseur = res
        return [epaisseur, hauteur, largeur, None]

    @catch_errors
    def _weight(self, product):
        """
        Return: float.
        """
        if not is_isbn(self.query):
            return

        res = product.find(class_="biblio-info").find_all('li')[1].span.text
        res = [it.strip() for it in res.split('mm')]  # dimensions, poid
        res = res[1]
        res = priceFromText(res)
        res = priceStr2Float(res)
        return res

    @catch_errors
    def _nb_pages(self, product):
        if not is_isbn(self.query):
            return

        res = product.find(class_="biblio-info").find('li').find(itemprop='numberOfPages').text
        res = priceFromText(res)
        return res

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        # return res
        pass

    @catch_errors
    def _lang(self, product):
        res = None
        elt = product.find(itemprop="inLanguage")
        if elt:
            res = elt.text.strip().lower()
        return res

    def search(self, *args, **kwargs):
        """Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        # nbr_results = self._nbr_results()
        for product in product_list:
            b = addict.Dict()
            b.authors = self._authors(product)
            b.authors_repr = ", ".join(b.authors) if b.authors else ""
            b.title = self._title(product)
            publishers = self._publisher(product)
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)  # TODO:
            # b.fmt = self._format(product)
            b.nb_pages = self._nb_pages(product)
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            dimensions = self._dimensions(product)
            if dimensions and len(dimensions) == 4:
                b.thickness, b.height, b.width, b.weight = dimensions
            b.weight = self._weight(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"
            b.lang = self._lang(product)

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(card, isbn=None):
    """Get a card (dictionnary) with 'details_url'.

    Gets additional data:
    - description

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.

    """
    return card


def _scrape_review(link):
    """
    DEPRECATED
    """
    return


@annotate(words=clize.Parameter.REQUIRED)
@autokwoargs()
def main(*words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    bklist = [postSearch(it) for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
