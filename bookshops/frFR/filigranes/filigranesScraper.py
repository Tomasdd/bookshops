#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
2020-09-30: we contacted the Banque du Livre and Dilicom to use
the FEL. The don't supply the WEB SERVICE but only the FTP. After
weeks of mis-communication and wait to have an FTP to try the service,
we observed that the FEL à la demande by FTP takes several minutes to
process our input files. This has been tested live with the Dilicom client support.
Their technical team confirms it is as the FTP works.
So it is not suitable for us.
We do web scraping untli they offer the WEB SERVICE.
"""

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "filigranes"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.filigranes.be"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.filigranes.be/produits?ts-obj=produits&ts-obj-produitsfamilles-id=&ts="
        #: advanced url (search for isbns): here the same one.
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = "https://www.filigranes.be/produits?ts-adv=1&ts="
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 12

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []
        try:
            # plist = self.soup.find(class_='produits')
            plist = self.soup.find_all('article')
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []
            # logging.debug("found plist: {}".format(len(plist)))
            return plist
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        pass

    @catch_errors
    def _details_url(self, product):
        """
        URL to the product's own page.
        """
        details_url = product.find(class_="pic").attrs["href"].strip().strip('?')
        details_url = self.SOURCE_URL_BASE + details_url
        return details_url

    @catch_errors
    def _title(self, product):
        title = product.h4.text.strip()
        # logging.info('title: {}'.format(title))
        return title.capitalize()

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        def split_to_list(txt):
            authors = txt.split(';')
            authors = [it for it in authors if it != ""]
            authors = [it.strip().capitalize() for it in authors]
            return authors

        authors = product.find(class_='label').find(class_='item').text
        authors = split_to_list(authors)
        # logging.info('authors: ' + ', '.join(a for a in authors))
        return authors

    @catch_errors
    def _img(self, product):
        img = product.img['src']
        return self.SOURCE_URL_BASE + img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        # XXX:
        pass

    def _price(self, product):
        "The real price (as float), without discounts"
        try:
            price = product.find(class_='prix').text.strip()
            price = priceFromText(price)
            price = priceStr2Float(price)
            assert isinstance(price, float)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        isbn = product['data-ref']
        return isbn

    @catch_errors
    def _description(self, product):
        """To get with postSearch.
        """
        pass

    @catch_errors
    def _date_publication(self, product):
        # unavailable.
        pass

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        pass  # unavailable

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        pass

    def search(self, *args, **kwargs):
        """Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        # nbr_results = self._nbr_results()
        for product in product_list:
            if product.find(class_='notfound'):
                # That's an article box for advertising: "contact us to find more".
                continue
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers) if publishers else ""
            b.card_type = self.TYPE_BOOK
            b.isbn = self._isbn(product)
            if b.isbn.startswith('97'):
                b.card_type = "book"
            else:
                b.card_type = "other"
            b.img = self._img(product)
            b.summary = self._description(product)
            b.availability = self._availability(product)

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(card, isbn=None):
    """Get a card (dictionnary) with 'details_url'.

    Gets additional data:
    - description

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.

    """
    return card


@annotate(words=clize.Parameter.REQUIRED, review='r')
@autokwoargs()
def main(review=False, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}".format(len(bklist))))
    bklist = [postSearch(it) for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
