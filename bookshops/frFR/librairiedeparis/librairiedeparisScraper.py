#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "librairiedeparis"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.librairie-de-paris.fr"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=simple&LIVREANCIEN=2&MOTS="
        #: advanced url (searcf for isbns)
        self.SOURCE_URL_ADVANCED_SEARCH = "https://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=appro&LIVREANCIEN=2&MOTS="
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = "&x=0&y=0"  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = "EDITEUR"
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = "NOMBRE"
        self.NBR_RESULTS = 12

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []
        try:
            plist = self.soup.find(class_='resultsList')
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []
            plist = plist.find_all('li', recursive=False)  # only direct <li> children.
            return plist
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        pass
        try:
            nbr_resultl_list = self.soup.find_all('div', class_='result_position')
            nbr_result = nbr_resultl_list[0].text.strip()
            res = nbr_result.split('sur')[-1]
            if not res:
                print('Error matching nbr_result')
            else:
                nbr = int(res.strip())
                self.NBR_RESULTS = nbr
                return nbr
        except Exception as e:
            logging.info("Could not fetch the nb of results: {}".format(e))

    @catch_errors
    def _details_url(self, product):
        details_url = product.find(class_="livre_titre").a.attrs["href"].strip()
        details_url = self.SOURCE_URL_BASE + details_url
        return details_url

    @catch_errors
    def _title(self, product):
        title = product.find(class_='livre_titre').text.strip()
        return title.capitalize()

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        authors = product.find(class_='livre_auteur').text  # xxx many authors ? see list_authors
        authors = authors.split('\n')
        # TODO: multiple authors
        authors = [it for it in authors if it != ""]
        authors = [it.strip().capitalize() for it in authors]
        return authors

    @catch_errors
    def _img(self, product):
        img = product.find(class_='zone_image').a.img['data-src']
        # Get a better resolution.
        try:
            img = img.replace('_m.jpg', '_75.jpg')
        except Exception as e:
            log.info("Could not get a better high cover image: {}".format(e))
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        try:
            pub = product.find(class_="editeur").text.split('-')[0].strip()
            # TODO: multiple publishers ?
            return [pub]
        except Exception as e:
            logging.error("Error scraping the publisher(s): {}".format(e))
        return []

    def _price(self, product):
        "The real price (as float), without discounts"
        try:
            price = product.find(class_='item_prix').text.strip()
            price = priceFromText(price)
            price = priceStr2Float(price)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        res = product.find(class_="editeur-collection-parution").text.split('\n')
        isbn = res[-2].strip()
        if not is_isbn(isbn):
            res = [it for it in res if is_isbn(it)]
            isbn = res[0]
        return isbn

    @catch_errors
    def _description(self, product):
        """To get with postSearch.
        """
        pass

    @catch_errors
    def _details(self, product):
        # looks deprecated.
        try:
            logging.warning("not up to date. Looks like unused anyway.")
            details_soup = product.getDetailsSoup()
            tech = details_soup.find(class_='technic')
            li = tech.find_all('li')

            details = {}
            for k in li:
                key = k.contents[0].strip().lower()
                if key == 'ean :':
                    details['isbn'] = k.em.text.strip()
                elif key == 'editeur :':
                    details['editor'] = k.em.text.strip()
                elif key == 'isbn :':
                    details['isbn'] = k.em.text.strip()

            if not details:
                logging.warning("Warning: we didn't get any details (isbn,...) about the book")
            return details

        except Exception as e:
            print(('Error on getting book details', e))

    @catch_errors
    def _date_publication(self, product):
        date_publication = product.find(class_="MiseEnLigne")
        if date_publication:
            # not available sometimes.
            date_publication = date_publication.text.strip()
        return date_publication

    @catch_errors
    def _availability(self, product):
        """Return: string.
        """
        availability = product.find(class_='item_stock')
        if availability:
            availability = availability.text.strip()
        return availability

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        FMT_POCKET = "Format poche"
        FMT_LARGE = "Grand format"
        fmt = product.find(class_='item_format')
        fmt = fmt.text.strip()
        res = None
        if FMT_LARGE.upper() in fmt.upper():
            res = FMT_LARGE
        elif FMT_POCKET.upper() in fmt.upper():
            res = FMT_POCKET

        return res

    def search(self, *args, **kwargs):
        """Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        # nbr_results = self._nbr_results()
        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(card, isbn=None):
    """Get a card (dictionnary) with 'details_url'.

    Gets additional data:
    - description

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.

    """
    return card


def _scrape_review(link):
    """
    DEPRECATED
    """
    return


@annotate(words=clize.Parameter.REQUIRED)
@autokwoargs()
def main(*words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    bklist = [postSearch(it) for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
